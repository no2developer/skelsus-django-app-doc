skel
====
Skeleton para desarrollo frontend configurado con GruntJS.

Requisitos
==============
NodeJS >= 0.10 (http://nodejs.org/)

Python >= 2.7 (https://www.python.org/downloads/)

Glue <= 0.9.2 (http://glue.readthedocs.org/en/latest/installation.html)

Grunt CLI (http://gruntjs.com/getting-started)

BrowserSync (http://www.browsersync.io/docs/command-line/)

Growl Windows (http://www.growlforwindows.com/gfw/)
