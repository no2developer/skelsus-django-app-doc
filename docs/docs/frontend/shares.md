## Share FB

Para el botón compartir utilizamos Feed Dialog de Facebook con el Javascript SDK de Facebook, para lo cual utilizamos nuestro módulo fb.js, el que nos permite renderizar el botón en un elemento html.  

Para su correcto funcionamiento, nuestro elemento html debe tener los siguientes atributos:

> data-appId!="El id de la aplicación de Facebook",  
> data-name!="El título del share",  
> data-link!="El enlace a donde apuntará nuestro compartir",  
> data-description!="La descripción de nuestro share",  
> data-picture!="La imagen relacionada a nuestro compartir"
> id!="share-fb"


Hay que considerar que estos elementos son por lo general seteados desde Backend y también se debe considerar que la URL de la imagen debe ser una URL completa y no relativa.  

El resultado nos debe arrojar:

![Share](/docs/images/shares/sharefb.png   "Ejemplo de Share con Feed Dialog")

El código Javascript que debemos incluir es el siguiente.

    fb.share({  
      el: document.getElementById('share-fb'),  // Indicar aquí el ID del elemento a renderizar.  
      tracking: {  
        category: '',  
        action: '',  
        label: ''  
      }  
    });  

Para entender mejor ésta parte, es importante haber leído previamente los módulos de RequireJS.
    

## Share TW

Para el botón compartir utilizamos Tweet Button de Twitter con la documentación de Twitter, para lo cual utilizamos nuestro módulo tw.js, el que nos permite renderizar el botón en un elemento html.  

Para su correcto funcionamiento, nuestro elemento html debe tener los siguientes atributos:  

> data-text="La descripción de nuestro tweet",  
> data-via="La cuenta de Twitter que será mencionada en el Tweet. No es obligatorio",   
> data-url="El enlace a donde apuntará nuestro tweet",   
> data-track="true"  
> id!="share-tw"

Hay que considerar que estos elementos son por lo general seteados desde Backend y también se debe considerar que la URL de la imagen debe ser una URL completa y no relativa.  

El resultado nos debe arrojar:

![Tweet](/docs/images/shares/sharetw.png   "Ejemplo de Tweet con Tweet Button")

El código Javascript que debemos incluir es el siguiente.

    tw.tweet({  
      el: document.getElementById('share-tw'),  // Indicar aquí el ID del elemento a renderizar.  
      custom: true,  
      tracking: {  
        category: '',  
        action: '',  
        label: ''  
      }  
    })  


Para entender mejor ésta parte, es importante haber leído previamente los módulos de RequireJS.  
