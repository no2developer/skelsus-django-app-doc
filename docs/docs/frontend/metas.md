
## Metas de Facebook / Open Graph

El Open Graph está determinado por etiquetas bajo la siguiente estructura:

    <meta property="og:property" content="value" />

Según el contenido a compartir deberemos indicar unas propiedades u otras. Las propiedades básicas que debe tener cualquier contenido son:

- **og:title**  
> Aquí debes incluir el título de tu contenido, tal y como quieres que aparezca en el grafo social.  

- **og:type**  
> La propiedad type sirve para especificar el tipo de contenido que estás compartiendo. Algunas opciones son: article, website, music.song, video.movie, etc. En función del tipo escogido puede ser necesario definir otra serie de propiedades adicionales. Por lo general es “Website”  

- **og:image**  
> Una URL a una imagen que represente el objeto en el grafo social.  

- **og:url**  
> La URL canónica del contenido a compartir.  


Hay que considerar que para las imágenes, por buenas prácticas, Facebook requiere una imagen de 1200 x 630px para una buena presentación en dispositivos de alta resolución, pero con 600 x 315px sería suficiente. OJO: Aquí debe colocarse la ruta completa de la imagen.

Ejemplo aplicado:


![PLA](/docs/images/metas/postlink.png   "Ejemplo de PLA con Open Graph")


Además de los metas básicos, existen varios opcionales:  

- **og:description**  
> Algunas líneas descriptivas para el objeto. Por buena practica no debemos de usar más de 200 caracteres.  

- **og:locale**  
> Declaramos el lugar de procedencia del objeto en el formato lenguaje_TERRITORIO; ejemplo, “es_PE”.  

- **og:site_name**  
> Si la web o la app es grande, por decirlo así, esta propiedad será la que identifique a todo el site; ejemplo, “Nodos Digital” ya que la web de referencia es “http://nodosdigital.pe”.  

- **og:audio**  
> La URL del audio que acompaña este objeto.  

- **og:video**  
> La URL del video que complementa el objeto creado.  

Para validar que los metas hayan sido colocados correctamente podemos utilizar la herramienta de Facebook:

[Facebook Open Graph Debugger](https://developers.facebook.com/tools/debug/ "Facebook Open Graph Debugger")


Fuente: [http://ogp.me/](http://ogp.me/ "Open Graph Documentation")


## Metas de Twitter / Twitter Card

Para Twitter es considerado importante trabajar con Twitter Card, sistema similar a Open Graph de Facebook. Trabaja con diversas propiedades, las cuales detallamos a continuación.

Estructura:  

    <meta name="twitter:property" content="value">  

Las propiedades básicas con las que debemos contar son las siguientes:

- **twitter:card**  
> Especifica el tipo de card. Lo ideal es trabajar con el valor **summary_large_image**. El valor por defecto es summary.  

- **twitter:title**        
> Especificar el título de la tarjeta. El valor máximo es 70 caracteres.

- **twitter:description**  
> Especificar la descripción de la tarjeta. El valor máximo es de 200 caracteres.

- **twitter:image**        
> Especificar la URL completa de la imagen que se mostrará. Considerar que si el valor de **twitter:card** es **summary_large_image**, la imagen deberá ser tamaño grande.
> Para ésta imagen podríamos utilizar la misma que se utiliza para **og:image** de Facebook.

